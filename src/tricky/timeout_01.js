import { log } from '../jslib.js';

//
// What is the output?
//
for (var i = 0; i < 3; ++i)
  setTimeout(function f() {
    log(i);
  }, 0);
