import { log } from '../jslib.js';

//
// What is the output?
//
for (var i = 0; i < 3; ++i)
  (function f(n) {
    setTimeout(function g() {
      log(n);
    }, 0);
  })(i);

